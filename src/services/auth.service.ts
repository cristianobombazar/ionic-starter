import {Injectable} from "@angular/core";
import {Usuario} from "../model/usuario.dto";
import {ApiConfig} from "../config/api.config";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class AuthService {

  URI = ApiConfig.baseUrl;

  constructor(public http: HttpClient) {
  }

  authenticate(usuario: Usuario){
    const headers = new HttpHeaders()
                        .set('Authorization', 'Basic YW5ndWxhcjpAbmd1bEByMA==')
                        .set('Content-Type', 'application/x-www-form-urlencoded');
    const body = `client=angular&username=${usuario.email}&password=${usuario.password}&grant_type=password`;
    return this.http.post(this.URI + '/oauth/token', body,
      {
        headers,
        withCredentials: true,
      }
    );
  }

}
