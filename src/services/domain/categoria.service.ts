import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import { ApiConfig } from "../../config/api.config";
import {Categoria} from "../../model/categoria.dto";
import {Observable} from "rxjs/Rx";

@Injectable()
export class CategoriaService {

  URI: string;

  constructor(public http: HttpClient){
    this.URI = ApiConfig.baseUrl;
  }

  findAll(): Observable<Categoria[]> {
    return this.http.get<Categoria[]>(this.URI + '/categoria');
  }


}
