import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CategoriaService} from "../../services/domain/categoria.service";
import {Categoria} from "../../model/categoria.dto";
import {ApiConfig} from "../../config/api.config";

/**
 * Generated class for the CategoriasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categorias',
  templateUrl: 'categorias.html',
})
export class CategoriasPage {

  items: Categoria[];
  buckerURL =  ApiConfig.baseBucketUrl;

  constructor(public navCtrl: NavController, public navParams: NavParams, public service: CategoriaService) {
  }

  ionViewDidLoad() {
    this.service.findAll().subscribe(response =>{
      this.items = response;
    }, error => {
       console.log(error);
    });
  }

}
