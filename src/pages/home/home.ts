import { Component } from '@angular/core';
import {IonicPage, MenuController, NavController} from 'ionic-angular';
import {CategoriasPage} from "../categorias/categorias";
import {Usuario} from "../../model/usuario.dto";
import {AuthService} from "../../services/auth.service";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  user: Usuario = {
    email: "",
    password: ""
  }

  constructor(public navCtrl: NavController, public menu: MenuController, private auth: AuthService) {
  }

  login() {
    this.auth.authenticate(this.user)
       .subscribe(response =>{
         console.log(response);
          this.navCtrl.setRoot('CategoriasPage');
       },
      error => {});
  }


  //life cycle ionic. When enter login page, disable swipe...
  ionViewWillEnter() {
    this.menu.swipeEnable(false);
  }

  //life cycle ionic. When leave login, enable swipe...
  ionViewDidLeave(){
    this.menu.swipeEnable(true);
  }


}
