import {HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs/Rx";
import {errorObject} from "rxjs/util/errorObject";
import {Injectable} from "@angular/core";

@Injectable()
export class ErrorInterceptor implements  HttpInterceptor{
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).catch( (error, caught) =>{
      let erroObj = error;
      if (erroObj.error){
        erroObj = erroObj.error;
      }
      if (!erroObj.status){
        erroObj = JSON.parse(erroObj);
      }
      console.log('Error detecado pelo interceptor');
      console.log(erroObj);
      return Observable.throw(error);
    }) as any;
  }
}

export const ErrorInterceptorProvider ={
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true,
}
